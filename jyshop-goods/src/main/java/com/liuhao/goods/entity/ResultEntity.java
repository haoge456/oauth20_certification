package com.liuhao.goods.entity;

import lombok.Data;

/**
 * 自定义实体类
 * 用来给前台返回信息
 * code  状态码
 * message 信息
 * object  返回对象
 */
@Data
public class ResultEntity {


    private String code;

    private String message;

    private Object object;


    public ResultEntity(String code, String message, Object object) {
        this.code = code;
        this.message = message;
        this.object = object;
    }

    /**
     * 成功方法
     * @param code
     * @param message
     * @param object
     * @return
     */
    public static ResultEntity ok(String code, String message, Object object) {
        return  new ResultEntity(code,message,object);
    }

    public static ResultEntity ok() {
        return  new ResultEntity("200","操作成功",null);
    }

    public static ResultEntity ok(String message) {
        return  new ResultEntity("200",message,null);
    }

    public static ResultEntity ok(Object object) {
        return  new ResultEntity("200","操作成功",object);
    }

    public static ResultEntity ok(String msg,Object o){
        return new ResultEntity("200",msg,o);

    }


    public static ResultEntity error(String code, String message, Object object) {
        return  new ResultEntity(code,message,object);
    }

    public static ResultEntity error() {
        return  new ResultEntity("500","操作失败",null);
    }

    public static ResultEntity error(String message) {
        return  new ResultEntity("500",message,null);
    }

    public static ResultEntity error(Object object) {
        return  new ResultEntity("500","操作失败",object);
    }


    public static ResultEntity error(String msg,Object o){
        return new ResultEntity("500",msg,o);

    }
}
