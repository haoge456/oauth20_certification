package com.liuhao.goods.controller;


import com.alibaba.fastjson.JSON;
import com.alipay.api.AlipayApiException;
import com.alipay.api.CertAlipayRequest;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePrecreateRequest;
import com.alipay.api.response.AlipayTradePrecreateResponse;
import com.liuhao.goods.config.alibaba.PayConfig;
import com.liuhao.goods.entity.ResultEntity;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.HashMap;
import java.util.Map;

@RestController
public class AlibabaPayContoller {


    @Autowired
    PayConfig payConfig;

    @Autowired
    RedisTemplate<String,String> redisTemplate;

    @RequestMapping("/alibaba/pay")
    public ResultEntity pay(String orderNo) throws AlipayApiException {

        //校验前台转过来的订单id
        if (StringUtils.isNotBlank(redisTemplate.opsForValue().get(orderNo))){
            return ResultEntity.ok("支付二维码返回成功",redisTemplate.opsForValue().get(orderNo));
        }
        CertAlipayRequest certAlipayRequest = new CertAlipayRequest();
        certAlipayRequest.setServerUrl(payConfig.getUrl());
        certAlipayRequest.setAppId(payConfig.getAppid());
        certAlipayRequest.setPrivateKey(payConfig.getAppPrivateKey());
        certAlipayRequest.setFormat("json");
        certAlipayRequest.setCharset("utf-8");
        certAlipayRequest.setSignType("RSA2");
        certAlipayRequest.setCertPath(payConfig.getAppCertPath());

        certAlipayRequest.setAlipayPublicCertPath(payConfig.getAlipayCertPath());
        certAlipayRequest.setRootCertPath(payConfig.getAlipayRootCertPath());
        DefaultAlipayClient alipayClient = new DefaultAlipayClient(certAlipayRequest);
//        AlipayClient alipayClient = new DefaultAlipayClient(payConfig.getUrl(), payConfig.getAppid(), payConfig.getAppPrivateKey(), "json", "utf-8", payConfig.getAlipayPublicKey(), "RSA2");

        AlipayTradePrecreateRequest request = new AlipayTradePrecreateRequest();//创建API对应的request类
        Map<String,String> maps = new HashMap<String,String>();
        maps.put("out_trade_no",orderNo);
        maps.put("total_amount","0.01");
        maps.put("subject","Iphone6 16G");
        maps.put("store_id","NJ_001");
        maps.put("timeout_express","3600m");
        request.setBizContent(JSON.toJSONString(maps));
        //设置回调的url地址 关键
        request.setNotifyUrl(payConfig.getCallBackUrl());
        AlipayTradePrecreateResponse response = alipayClient.certificateExecute(request);
        System.out.print(response.getBody());
        String qrcode = response.getQrCode();
        redisTemplate.opsForValue().set(orderNo,qrcode);
        return ResultEntity.ok("支付二维码返回成功",redisTemplate.opsForValue().get(orderNo));

    }
}
