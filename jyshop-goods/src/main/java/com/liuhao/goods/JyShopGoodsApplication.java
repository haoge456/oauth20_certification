package com.liuhao.goods;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan(basePackages = "com.liuhao.goods.mapper")
public class JyShopGoodsApplication {


    public static void main(String[] args) {

        SpringApplication.run(JyShopGoodsApplication.class,args);
    }
}
