package com.liuhao.goods.service;

import com.liuhao.goods.entity.Goods;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuhao
 * @since 2020-05-22
 */
public interface IGoodsService extends IService<Goods> {

}
