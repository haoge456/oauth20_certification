package com.liuhao.goods.mapper;

import com.liuhao.goods.entity.Goods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuhao
 * @since 2020-05-22
 */
public interface GoodsMapper extends BaseMapper<Goods> {

}
