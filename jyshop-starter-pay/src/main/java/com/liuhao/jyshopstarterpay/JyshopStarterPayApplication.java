package com.liuhao.jyshopstarterpay;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JyshopStarterPayApplication {


    public static void main(String[] args) {
        SpringApplication.run(JyshopStarterPayApplication.class,args);
    }
}
