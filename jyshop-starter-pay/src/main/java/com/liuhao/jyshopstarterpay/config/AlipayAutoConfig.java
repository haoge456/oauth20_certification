package com.liuhao.jyshopstarterpay.config;

import com.liuhao.jyshopstarterpay.service.AlipayService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration()
@ConditionalOnClass({AlipayService.class})
public class AlipayAutoConfig {



    @Bean
    @ConditionalOnProperty(prefix="alipay",value = "enable",havingValue = "true")
    public AlipayService alipayService(){
        return new AlipayService();
    }
}
