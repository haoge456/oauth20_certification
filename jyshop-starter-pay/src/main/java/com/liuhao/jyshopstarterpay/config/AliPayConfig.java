package com.liuhao.jyshopstarterpay.config;

import com.liuhao.jyshopstarterpay.utils.PropertiesUtil;

public class AliPayConfig {

    public static  final  String config = "alipay";

    public static final String url =  PropertiesUtil.getValue(config, "alipay.url");
    public static final String APP_ID =  PropertiesUtil.getValue(config, "alipay.appid");
    public static final String APP_PRIVATE_KEY =  PropertiesUtil.getValue(config, "alipay.appPrivateKey");
    public static final String ALIPAY_PUBLIC_KEY =  PropertiesUtil.getValue(config, "alipay.alipayPublicKey");
    public static final String APP_CERT_PATH =  PropertiesUtil.getValue(config, "alipay.appCertPath");
    public static final String ALIPAY_CERT_PATH =  PropertiesUtil.getValue(config, "alipay.alipayCertPath");
    public static final String ALIPAY_ROOTCERT_PATH =  PropertiesUtil.getValue(config, "alipay.alipayRootCertPath");
    public static final String CALL_BACK_URL =  PropertiesUtil.getValue(config, "alipay.callBackUrl");
}
