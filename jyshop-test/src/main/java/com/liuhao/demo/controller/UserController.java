package com.liuhao.demo.controller;


import com.liuhao.demo.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author liuhao
 * @since 2020-05-21
 */
@RestController
@RequestMapping("/auth/tUser")
public class UserController {

    @Autowired
    private IUserService iUserService;

    @RequestMapping("/list")
    public Object list(){

        return iUserService.list();
    }

    @GetMapping("/user/info")
    public Principal user(Principal member) {
        return member;
    }
}

