package com.liuhao.demo.service;

import com.liuhao.demo.entity.TUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuhao
 * @since 2020-05-21
 */
public interface IUserService extends IService<TUser> {

}
