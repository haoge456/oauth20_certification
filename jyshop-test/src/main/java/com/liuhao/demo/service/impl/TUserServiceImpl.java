package com.liuhao.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.liuhao.demo.entity.TUser;
import com.liuhao.demo.mapper.TUserMapper;
import com.liuhao.demo.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuhao
 * @since 2020-05-21
 */
@Service
@Slf4j
public class TUserServiceImpl extends ServiceImpl<TUserMapper, TUser> implements IUserService, UserDetailsService {

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        log.info("用户进行认证的用户名为：{}",s);

        QueryWrapper<TUser> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("user_name",s);
        TUser tUser1 = this.baseMapper.selectOne(objectQueryWrapper);
        if (tUser1==null){
            throw  new UsernameNotFoundException("用户名不存在");
        }
        return new User(s,tUser1.getUserPassword(), AuthorityUtils.createAuthorityList("admin"));

    }
}
