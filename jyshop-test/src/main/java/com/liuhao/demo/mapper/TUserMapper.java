package com.liuhao.demo.mapper;

import com.liuhao.demo.entity.TUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuhao
 * @since 2020-05-21
 */
public interface TUserMapper extends BaseMapper<TUser> {

}
