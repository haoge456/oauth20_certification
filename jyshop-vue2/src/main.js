import Vue from 'vue'
import App from './App.vue'
import store from './store'

//导入
import router from './router'

import QRCode from "qrcode"; //引入生成二维码插件
Vue.prototype.QRCode=QRCode;
//导入elementui
import ElementUI from 'element-ui'
import  'element-ui/lib/theme-chalk/index.css';
//Vue对象的全局use使用
Vue.use(ElementUI)
//导入axios，VueAxios
import axios from 'axios'
import VueAxios from 'vue-axios'
//使用VueAxios axios
Vue.use(VueAxios, axios)
Vue.config.productionTip = false
//解决axios的跨域问题
//axios.default.options.withCredentials=true
axios.defaults.withCredentials=true

//全局守卫   不管你走什么样的路由连接，都必须经过我处理 过滤器 拦截器
router.beforeEach((to,from,next)=>{
	//console.log("离开的路由："+from.name+"@@"+from.path+"@@@@@@"+from.component);
	//console.log("进来的路由："+to.name+"@@"+to.path+"@@@@@@"+to.component);
	//判断路由是否进入menu资源
	 if(to.path=='/'){
	     localStorage.removeItem('name');
	     next();
	  }else{
	    if(!localStorage.getItem('name')){
	      next("/");
	      return;
	    }
		}
	if(to.path =='/home'){				
		
		//根据用户名查询对应的菜单资源
		axios.get('http://localhost:9991/qx/menu/selectMenuListByUsername',{
			
			params: {"access_token":localStorage.getItem("access_token"),"username":localStorage.getItem("name")}
		}).then((res)=>{
			//获取菜单资源
			var menuArray = res.data;
			//把资源对象转换成VueRouter对象
			var newRouters = setRouters(menuArray);
			console.log("把资源对象转换成VueRouter对象"+newRouters);
			//根据路由对象设置参数   下标的位置一定要根据 router/index.js的router数组决定的
			router.options.routes[1].children = newRouters;
			//加入到路由对象
			router.addRoutes(router.options.routes);
			
			next();
			
		});
		
	}
	else{
		next();
	}
	
	
})
export const setRouters = (menuArray)=>{
	//定义转换的结果
	let newRouters = [];
	//遍历数组 一级菜单
    for(let i=0;i<menuArray.length;i++){
		//二级菜单
		let children =[];
		//遍历二级菜单
		if(menuArray[i].menuList && menuArray[i].menuList instanceof Array){
			//递归处理
			children = setRouters(menuArray[i].menuList);
		}
		//创建路由对象
		let newRouter = {
			name:menuArray[i].name,
			path:menuArray[i].path,
			icon:menuArray[i].icon,
			children:children,
			/* component(resolve){
			     if(element.component.startsWith('User')){
			             require(['./views/' + element.component + '.vue'], resolve)
			           }else if(element.component.startsWith('Role')){
			           require(['./views/' + element.component + '.vue'], resolve)  
			           }
			              }, */
		};
		
		//创建路由的comppnent组件
		let comppnentName = menuArray[i].component;
		
		newRouter.component=()=>import(/* webpackChunkName: "about" */ './views/'+comppnentName+'.vue');
		//存入到转换的结果
		newRouters.push(newRouter);
	}

     return  newRouters;

}


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
