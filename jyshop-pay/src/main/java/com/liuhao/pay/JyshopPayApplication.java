package com.liuhao.pay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication
public class JyshopPayApplication {

    public static void main(String[] args) {

        SpringApplication.run(JyshopPayApplication.class,args);
    }

}
