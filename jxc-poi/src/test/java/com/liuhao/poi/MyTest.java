package com.liuhao.poi;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.liuhao.poi.mapper.Sale_list_goodsMapper;
import com.liuhao.poi.service.ISale_list_goodsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MyTest {


    @Autowired
    private ISale_list_goodsService sale_list_goodsMapper;
    @Test
    public void test1(){
        Page page = new Page(1,3);

        IPage iPage = sale_list_goodsMapper.selectSaleGoods(page);

        System.err.println(iPage.getRecords());
    }
}
