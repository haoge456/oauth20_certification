package com.liuhao.poi.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.liuhao.poi.entity.SaleListGoodsVo;
import com.liuhao.poi.entity.Sale_list_goods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuhao
 * @since 2020-06-04
 */
public interface Sale_list_goodsMapper extends BaseMapper<Sale_list_goods> {

    IPage selectSaleGoods(Page page);
}
