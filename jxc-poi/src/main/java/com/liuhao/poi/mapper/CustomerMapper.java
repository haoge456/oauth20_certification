package com.liuhao.poi.mapper;

import com.liuhao.poi.entity.Customer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuhao
 * @since 2020-06-04
 */
public interface CustomerMapper extends BaseMapper<Customer> {

}
