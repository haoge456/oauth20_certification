package com.liuhao.poi.mapper;

import com.liuhao.poi.entity.Sale_list;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuhao
 * @since 2020-06-04
 */
public interface Sale_listMapper extends BaseMapper<Sale_list> {

}
