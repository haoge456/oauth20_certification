package com.liuhao.poi.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.liuhao.poi.entity.SaleListGoodsVo;
import com.liuhao.poi.service.ISale_list_goodsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.beans.IntrospectionException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author liuhao
 * @since 2020-06-04
 */
@RestController
@RequestMapping("/saleListGoods")
@Slf4j
public class Sale_list_goodsController {


    @Autowired
    private ISale_list_goodsService iSale_list_goodsService;


    @RequestMapping("/list")
    public IPage list(@RequestParam(value = "pageNum",defaultValue = "1")Integer pageNum,
                      @RequestParam(value = "pageSize",defaultValue = "3")Integer pageSize
                      ){

        Page page = new Page(pageNum,pageSize);


         log.info("开始查询数据:{},{}",pageNum,pageSize);
        return iSale_list_goodsService.selectSaleGoods(page) ;
    }


    @RequestMapping("/downLoadGoodsExcel")
    public ResponseEntity<byte[]> downLoadGoodsExcel(@RequestParam("pageNum")Integer pageNum,@RequestParam("pageSize")Integer pageSize) throws ClassNotFoundException, IntrospectionException, IllegalAccessException, ParseException, InvocationTargetException, IOException {
        XSSFWorkbook xssfSheets = iSale_list_goodsService.downLoadGoodsExcel(pageNum,pageSize);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        xssfSheets.write(byteArrayOutputStream);

        HttpHeaders httpHeaders =  new HttpHeaders();
        httpHeaders.setContentDispositionFormData("attachment", new String("商品销售统计表.xls".getBytes("UTF-8"), "ISO-8859-1"));
        httpHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        log.info("返回结果ResponseEntity");
        return new ResponseEntity(byteArrayOutputStream.toByteArray(),httpHeaders, HttpStatus.CREATED);
    }

    @RequestMapping("/updateLoad")
    public void uploadExcel(MultipartFile file) throws Exception {

        iSale_list_goodsService.uploadExcel(file.getInputStream(),file.getOriginalFilename());
    }

}

