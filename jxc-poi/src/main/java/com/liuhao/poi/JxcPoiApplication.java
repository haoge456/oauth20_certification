package com.liuhao.poi;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.liuhao.poi.mapper")
public class JxcPoiApplication {

    public static void main(String[] args) {

        SpringApplication.run(JxcPoiApplication.class,args);
    }
}
