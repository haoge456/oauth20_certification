package com.liuhao.poi.service;

import com.liuhao.poi.entity.Goodstype;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuhao
 * @since 2020-06-04
 */
public interface IGoodstypeService extends IService<Goodstype> {

}
