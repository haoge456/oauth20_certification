package com.liuhao.poi.service.impl;

import com.liuhao.poi.entity.Customer;
import com.liuhao.poi.mapper.CustomerMapper;
import com.liuhao.poi.service.ICustomerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuhao
 * @since 2020-06-04
 */
@Service
public class CustomerServiceImpl extends ServiceImpl<CustomerMapper, Customer> implements ICustomerService {

}
