package com.liuhao.poi.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.liuhao.poi.entity.SaleListGoodsVo;
import com.liuhao.poi.entity.Sale_list_goods;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.beans.IntrospectionException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuhao
 * @since 2020-06-04
 */
public interface ISale_list_goodsService extends IService<Sale_list_goods> {


    IPage selectSaleGoods(Page page);


     XSSFWorkbook downLoadGoodsExcel(Integer pageNum,Integer pageSize) throws InvocationTargetException, ClassNotFoundException, IntrospectionException, ParseException, IllegalAccessException;


    public void uploadExcel(InputStream inputStream, String getOriginalFilename) throws Exception;
}
