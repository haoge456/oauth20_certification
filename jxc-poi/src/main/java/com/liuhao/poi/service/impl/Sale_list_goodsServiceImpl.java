package com.liuhao.poi.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.liuhao.poi.entity.*;
import com.liuhao.poi.mapper.CustomerMapper;
import com.liuhao.poi.mapper.Sale_list_goodsMapper;
import com.liuhao.poi.service.ICustomerService;
import com.liuhao.poi.service.IGoodstypeService;
import com.liuhao.poi.service.ISale_listService;
import com.liuhao.poi.service.ISale_list_goodsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.liuhao.poi.utils.ExcelBean;
import com.liuhao.poi.utils.ExcelUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.beans.IntrospectionException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuhao
 * @since 2020-06-04
 */
@Service
@Slf4j
public class Sale_list_goodsServiceImpl extends ServiceImpl<Sale_list_goodsMapper, Sale_list_goods> implements ISale_list_goodsService {

    @Resource
    private Sale_list_goodsMapper sale_list_goodsMapper;
    @Override
    public IPage selectSaleGoods(Page page){

        return sale_list_goodsMapper.selectSaleGoods(page);
    }

    public XSSFWorkbook downLoadGoodsExcel(Integer pageNum,Integer pageSize) throws InvocationTargetException, ClassNotFoundException, IntrospectionException, ParseException, IllegalAccessException {

       // PageHelper.startPage(goodDto.getPageNo(),goodDto.getPageSize());

        Page page = new Page<>(pageNum,pageSize);
      //  Wrapper wrapper = new EntityWrapper();
        /*if (StringUtils.isNotBlank(goodDto.getName())){
            wrapper.like("name",goodDto.getName());
        }*/

        IPage iPage = sale_list_goodsMapper.selectSaleGoods(page);
        List records = iPage.getRecords();
        //List<TGoods> tGoods = this.selectList(wrapper);
        //PageInfo<TGoods> pageInfo = new PageInfo<>(tGoods);
        Map<Integer, List<ExcelBean>> map = new HashMap<Integer, List<ExcelBean>>();

        List<ExcelBean> excelBeans = new ArrayList<>();
        excelBeans.add(new ExcelBean("订单号","saleNumber",0));
        excelBeans.add(new ExcelBean("日期","saleDate",0));
        excelBeans.add(new ExcelBean("客户","contact",0));
        excelBeans.add(new ExcelBean("商品编号","code",0));
        excelBeans.add(new ExcelBean("商品名称","name",0));
        excelBeans.add(new ExcelBean("商品型号","model",0));
        excelBeans.add(new ExcelBean("商品类别","typeName",0));
        excelBeans.add(new ExcelBean("单位","unit",0));
        excelBeans.add(new ExcelBean("单价","price",0));
        excelBeans.add(new ExcelBean("数量","num",0));
        excelBeans.add(new ExcelBean("总金额","total",0));
        map.put(0,excelBeans);
        log.info("开始执行创建Excel表格方法---------:{}",map);
        //XSSFWorkbook book = ExcelUtils.createExcelFile(TGoods.class, pageInfo.getList(), map, "商品列表");
        XSSFWorkbook book = ExcelUtils.createExcelFile(SaleListGoodsVo.class, records, map, "商品销售统计");
        return book;
    }

    @Resource
    private CustomerMapper customerMapper;

    @Autowired
    private IGoodstypeService iGoodstypeService;

    @Autowired
    private ISale_listService iSale_listService;


    @Override
    public void uploadExcel(InputStream inputStream, String getOriginalFilename) throws Exception {
        log.info("开始进入ExcelUtils-----------------------{}，{}",inputStream,getOriginalFilename);
        List<List<Object>> bankListByExcel = ExcelUtils.getBankListByExcel(inputStream, getOriginalFilename);
        //List<SaleListGoodsVo> goodsList = new ArrayList<>();
        log.info("开始循环添加数据-----------------------");
        for (List<Object> objects : bankListByExcel) {
            //SaleListGoodsVo tGoods = new SaleListGoodsVo();
            //tGoods.setSaleNumber(String.valueOf(objects.get(1)));
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date saleDate = simpleDateFormat.parse(String.valueOf(objects.get(1)));
            //tGoods.setSaleDate(saleDate);
            //tGoods.setContact(String.valueOf(objects.get(3)));
            //tGoods.setCode(String.valueOf(objects.get(4)));
            //tGoods.setName(String.valueOf(objects.get(5)));
            //tGoods.setModel(String.valueOf(objects.get(6)));
            //tGoods.setTypeName(String.valueOf(objects.get(7)));
            //tGoods.setUnit(String.valueOf(objects.get(8)));
            //tGoods.setPrice(Float.parseFloat(String.valueOf(objects.get(9))));
            //tGoods.setNum(Integer.parseInt(String.valueOf(objects.get(10))));
            //tGoods.setTotal(Float.parseFloat(String.valueOf(objects.get(11))));
           // goodsList.add(tGoods);

           //对 t_customer 进行添加
            Customer customer = new Customer();

            customer.setContact(String.valueOf(objects.get(2)));
            int insert = customerMapper.insert(customer);

            // 对t_sale_list 进行添加
            //Sale_list sale_list = new Sale_list();
            //sale_list.setCustomer_id(customer.getId());
            //sale_list.setSale_date(saleDate);
            //sale_list.setSale_number(String.valueOf(objects.get(0)));
            //boolean save = iSale_listService.save(sale_list);
            //对t_goodstype 进行添加

            Goodstype goodstype = new Goodstype();
            goodstype.setName(String.valueOf(objects.get(6)));
            iGoodstypeService.save(goodstype);

            //对t_sale_list_goods进行添加
            Sale_list_goods sale_list_goods = new Sale_list_goods();
            sale_list_goods.setCode(String.valueOf(objects.get(3)));
            sale_list_goods.setModel(String.valueOf(objects.get(5)));
            sale_list_goods.setNum(Integer.parseInt(String.valueOf(objects.get(9))));
            sale_list_goods.setPrice(Float.parseFloat(String.valueOf(objects.get(8))));
            //sale_list_goods.setSale_list_id(sale_list.getId());
            //sale_list_goods.setType_id(goodstype.getId());
            sale_list_goods.setTotal(Float.parseFloat(String.valueOf(objects.get(10))));
            sale_list_goods.setUnit(String.valueOf(objects.get(7)));
            sale_list_goods.setName(String.valueOf(objects.get(4)));
            sale_list_goodsMapper.insert(sale_list_goods);
        }


    }
}

