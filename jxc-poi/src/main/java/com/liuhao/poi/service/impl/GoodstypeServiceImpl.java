package com.liuhao.poi.service.impl;

import com.liuhao.poi.entity.Goodstype;
import com.liuhao.poi.mapper.GoodstypeMapper;
import com.liuhao.poi.service.IGoodstypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuhao
 * @since 2020-06-04
 */
@Service
public class GoodstypeServiceImpl extends ServiceImpl<GoodstypeMapper, Goodstype> implements IGoodstypeService {

}
