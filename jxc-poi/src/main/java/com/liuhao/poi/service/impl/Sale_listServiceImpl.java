package com.liuhao.poi.service.impl;

import com.liuhao.poi.entity.Sale_list;
import com.liuhao.poi.mapper.Sale_listMapper;
import com.liuhao.poi.service.ISale_listService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuhao
 * @since 2020-06-04
 */
@Service
public class Sale_listServiceImpl extends ServiceImpl<Sale_listMapper, Sale_list> implements ISale_listService {

}
