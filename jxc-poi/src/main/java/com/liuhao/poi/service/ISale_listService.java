package com.liuhao.poi.service;

import com.liuhao.poi.entity.Sale_list;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuhao
 * @since 2020-06-04
 */
public interface ISale_listService extends IService<Sale_list> {

}
