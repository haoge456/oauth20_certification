package com.liuhao.qx.mapper;

import com.liuhao.qx.entity.Menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuhao
 * @since 2020-05-26
 */
public interface MenuMapper extends BaseMapper<Menu> {

    List<Menu> selectMenuListByUsername(String username);
}
