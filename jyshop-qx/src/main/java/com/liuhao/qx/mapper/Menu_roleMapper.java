package com.liuhao.qx.mapper;

import com.liuhao.qx.entity.Menu_role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuhao
 * @since 2020-05-26
 */
public interface Menu_roleMapper extends BaseMapper<Menu_role> {

}
