package com.liuhao.qx.mapper;

import com.liuhao.qx.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuhao
 * @since 2020-05-26
 */
public interface RoleMapper extends BaseMapper<Role> {

}
