package com.liuhao.qx.controller;


import com.liuhao.qx.entity.Menu;
import com.liuhao.qx.service.IMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author liuhao
 * @since 2020-05-26
 */
@RestController
@RequestMapping("/menu")
public class MenuController {

    @Autowired
    private IMenuService iMenuService;

    @RequestMapping("/selectMenuListByUsername")
    public List<Menu> selectMenuListByUsername(String username){

        return iMenuService.selectMenuListByUsername(username);
    }

}

