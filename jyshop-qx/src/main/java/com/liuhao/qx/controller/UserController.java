package com.liuhao.qx.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author liuhao
 * @since 2020-05-26
 */
@Controller
@RequestMapping("/user")
public class UserController {

}

