package com.liuhao.qx.service;

import com.liuhao.qx.entity.Menu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuhao
 * @since 2020-05-26
 */
public interface IMenuService extends IService<Menu> {

    List<Menu> selectMenuListByUsername(String username);
}
