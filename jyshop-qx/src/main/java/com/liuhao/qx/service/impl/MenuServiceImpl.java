package com.liuhao.qx.service.impl;

import com.liuhao.qx.entity.Menu;
import com.liuhao.qx.mapper.MenuMapper;
import com.liuhao.qx.service.IMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuhao
 * @since 2020-05-26
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {

    @Resource
    private MenuMapper menuMapper;
    @Override
    public List<Menu> selectMenuListByUsername(String username) {
        return menuMapper.selectMenuListByUsername(username);
    }
}
