package com.liuhao.qx.service.impl;

import com.liuhao.qx.entity.User;
import com.liuhao.qx.mapper.UserMapper;
import com.liuhao.qx.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuhao
 * @since 2020-05-26
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
