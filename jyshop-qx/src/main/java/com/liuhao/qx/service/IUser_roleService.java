package com.liuhao.qx.service;

import com.liuhao.qx.entity.User_role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuhao
 * @since 2020-05-26
 */
public interface IUser_roleService extends IService<User_role> {

}
