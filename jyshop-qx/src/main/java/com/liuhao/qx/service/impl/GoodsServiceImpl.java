package com.liuhao.qx.service.impl;

import com.liuhao.qx.entity.Goods;
import com.liuhao.qx.mapper.GoodsMapper;
import com.liuhao.qx.service.IGoodsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuhao
 * @since 2020-05-26
 */
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods> implements IGoodsService {

}
