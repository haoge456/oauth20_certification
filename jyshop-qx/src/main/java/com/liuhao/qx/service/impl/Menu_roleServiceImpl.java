package com.liuhao.qx.service.impl;

import com.liuhao.qx.entity.Menu_role;
import com.liuhao.qx.mapper.Menu_roleMapper;
import com.liuhao.qx.service.IMenu_roleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuhao
 * @since 2020-05-26
 */
@Service
public class Menu_roleServiceImpl extends ServiceImpl<Menu_roleMapper, Menu_role> implements IMenu_roleService {

}
