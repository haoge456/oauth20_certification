package com.liuhao.qx.service.impl;

import com.liuhao.qx.entity.Role;
import com.liuhao.qx.mapper.RoleMapper;
import com.liuhao.qx.service.IRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuhao
 * @since 2020-05-26
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

}
