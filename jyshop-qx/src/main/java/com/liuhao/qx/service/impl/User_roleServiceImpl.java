package com.liuhao.qx.service.impl;

import com.liuhao.qx.entity.User_role;
import com.liuhao.qx.mapper.User_roleMapper;
import com.liuhao.qx.service.IUser_roleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuhao
 * @since 2020-05-26
 */
@Service
public class User_roleServiceImpl extends ServiceImpl<User_roleMapper, User_role> implements IUser_roleService {

}
