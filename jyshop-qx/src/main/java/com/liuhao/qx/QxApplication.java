package com.liuhao.qx;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.liuhao.qx.mapper")
public class QxApplication {

    public static void main(String[] args) {
        SpringApplication.run(QxApplication.class,args);
    }
}
